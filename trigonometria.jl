# MAC0110 - MiniEP7
# Gabriel Fernandes Mota - 11796402

<funções da parte 1>

function fatorial(n)
        f = 1
        for i = 0:(n - 1)
                f = f*(n-i)
        end
        return(f)
end

function sin(x)
	seno = 0
	for i = 0:10
		p1 = ((-1)^(i)) * (x^(2*i+1))
		seno = seno + BigFloat(p1) / fatorial(2i+1)
	end
	return(big(seno))
end

function cos(x)
        cosseno = 0
        for i = 0:10
                p1 = ((-1)^i) * (x^(2i))
		cosseno = cosseno + BigFloat(p1) / fatorial(2i)
        end
        return(big(cosseno))
end

function bernoulli(n)
        n += 1
        A = Vector{Float64}(undef, n + 1)
        for m = 0 : n
                A[m + 1] = 1 // (m + 1)
                for j = m : -1 : 1
                        A[j] = j * (A[j] - A[j + 1])
                end
        end
        return A[1]
end

function tan(x)
        tangente = 0
        for i = 0:10
		p1 = (2^(2*i)) * ( (2^(2*i)) - 1)
		p2 = x^(2*i - 1)
                tangente = tangente + BigFloat(p1) * bernoulli(i) * BigFloat(p2) / fatorial(2*i)
        end
        return(big(tangente))
end

<funções da parte 2>

function quaseigual(v1, v2)
	erro = 0.0001
	igual = abs(v1 - v2)
	if igual <= erro
        	return true
	else
        	return false
	end
end

function check_sin(value, x)
	return(quaseigual(value, sin(x)))
end

function check_cos(value, x)
	return(quaseigual(value, cos(x)))
end

function check_tan(value, x)
	return(quaseigual(value, tan(x)))
end

function taylor_sin(x)
        seno = 0
        for i = 0:10
                p1 = ((-1)^(i)) * (x^(2*i+1))
                seno = seno + BigFloat(p1) / fatorial(2i+1)
        end
        return(big(seno))
end

function taylor_cos(x)
        cosseno = 0
        for i = 0:10
                p1 = ((-1)^i) * (x^(2i))
                cosseno = cosseno + BigFloat(p1) / fatorial(2i)
        end
        return(big(cosseno))
end

function taylor_tan(x)
        tangente = 0
        for i = 0:10
                p1 = (2^(2*i)) * ( (2^(2*i)) - 1)
                p2 = x^(2*i - 1)
                tangente = tangente + BigFloat(p1) * bernoulli(i) * BigFloat(p2) / fatorial(2*i)
        end
        return(big(tangente))
end

function test()
	@test quaseigual(sin(pi/3), 0.866)
	@test quaseigual(sin(pi/4), 0.7071)
	@test quaseigual(sin(pi/6), 0.5)
	@test quaseigual(cos(pi/3), 0.5)
	@test quaseigual(cos(pi/4), 0.7071)
	@test quaseigual(cos(pi/6), 0.866)
	@test quaseigual(tan(pi/6), 0.5162)
	@test check_sin(0.5, pi/6)
	@test !check_sin(0.5, pi/3)
	@test check_cos(0.5, pi/3)
	@test !check_cos(0.5, pi/6)
	@test check_tan(0.5162, pi/6)
	println("Fim dos teste!")
end
